# Cloud developer position

At Midships, we don’t do ordinary.  We want to change how tech is done across the industry and to do that we need to hire more like minded folks who are GREAT at tech. This means you need to be honest, pragmatic and focused on achieving  customers’ outcomes.  It also means you will be regularly learning new technologies (on the job) and regularly expected to do the impossible with tech resources available to you.
We are looking to hire a number of experienced cloud developers over the next 3 months and are looking for individuals that:

- Have a good grasp of English (written and speaking)
- Have worked with Docker, Kubernetes and at least one major Cloud Service Provider
- Understand DevSecOps and have deployed containers using Helm / Yaml
- Good at scripting

We don’t really care about your history just your future if you join us. We care about your personality (i.e. you need to fit into the team which shouldn’t be hard given how diverse we are!) and we care about your tech capability. 
If you want to go straight to the final interview (where we will test your capability re the above), follow the instructions to solve the above challange and you will be able to contact one of the team managers, who will set you the challenge and on completion barring any personality concerns will make you an offer.
In case you are interested, there are many benefits working with Midships, they include:

- Competitive Salary
- A fun and informal workplace
- Generous holiday allowance 
- Challenging projects with real responsibility
- Opportunity to work across new cutting edge technologies
- Be part of a great team of exceptional individuals
- Profit sharing opportunities after 2 years

## The challenge

In order to schedule your interview you will need to complete the following challenge which will direct you to the detailed instructions on how to schedule an interview with us.

The challenge is designed of three parts to test your technical knowledge and your analytical and design skills, the ultimate goal is to complete the tasks to be able to schedule an interview appointment with
**Midships**.

Please be prepared and keep all the artefacts you create and write down your notes, ideas and questions to be reviewed and discussed during the interview. Before starting with the challenge, make sure that:

-   You have a stable internet access
-   You have access to a local (microk8s,minikube) or managed K8S cluster on your favourite cloud provider
-   You complete the exercises in sequential order and try to meet all the requirements for each task

Best wishes from **Midships** team!


#### Part 1

Create a DB backend using your preferred backend (Postgres/MySQL/MongoDB,
etc…) in a new namespace called ***midships-challenge***

The DB backend should contain a database with a table with the following
structure (example entry is provided as example):

| Username | Password | Timestamp |
|----------|----------|-----------|
|   test       |  Password01!        |     27052021-19:31:14      |

Please note that:
- Username should be the primary key
- Timestamp should be datetime format  *ddmmyyyy–HH:mm:ss*

#### Part 2

Deploy a web server (Nginx/Tomcat/Apache,…) of your choice in the k8s cluster which
will be hosting the web application to deploy in **Part 3**

#### Part 3

Create a simple WebApp using your favourite language (NodeJS/PHP/HTML/...) to do the following:

-   Create a simple registration form to register a new user and store the data in the backend DB created in **Part1**. You will just need to collect the username and password and automatically generate the timestamp to be included in the database entry.
-   Login with the newly registered user
-   On successful login, redirect the user to the following URL **https://www.midships.io/job-test-done**. This page contains the detailed instructions on how to setup an interview with one of our founders!

*Note: Make sure that your web application URL is accessible via a valid ingress
route configured in the cluster*



## About Midships

We are a UK based company that operates globally to provide cloud focused solutions and consulting services. We use our hands-on knowledge of Cloud, DevSecOps, Digital Transformation, and CIAM for accelerated & customer-centric delivery.
    
